# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Random
from random import randint

# Waka
from waka.module import WakaModule


class Module(WakaModule):

    def call(self):
        """ Call this method from channels

        Returns
        -------
        str or None
            Response message
        """

        if len(self.response) > 1:

            # User wants to slaps himself
            if self.current_user == self.response[1]:
                return self.message_himself % self.current_user

            # Slaps a specific person
            return self.message_slaps % (
                self.current_user, ' '.join(self.response[1:]))

        if self.users:
            index = randint(0, len(self.users) - 1)

            # Slaps a random person
            return self.message_random % (self.current_user, self.users[index])

        # There is nobody in there
        return self.message_nobody % self.current_user
