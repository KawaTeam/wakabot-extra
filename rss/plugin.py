# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Feedparser
from feedparser import parse as feed_parse

# URLLib
from urllib import error as urllib_error

# Waka
from waka.module import WakaModule


class Module(WakaModule):

    def call(self):
        """ Call this method from channels

        Returns
        -------
        str or None
            Response message
        """

        if not self.config.has_section("websites"):
            return self.message_empty

        if len(self.response) <= 1:
            return self.message_available % (
                ", ".join(self.config.options("websites")))

        max_item = self.variable_max

        # Manage a specific amount of item to display
        if len(self.response) > 2:
            try:
                max_item = int(self.response[2])

                if max_item > self.variable_max or max_item < 0:
                    max_item = self.variable_max

            except Exception:
                max_item = self.variable_max

        # Retrieve website link
        link = self.config.get(
            "websites", self.response[1].lower(), fallback=None)

        if link is not None:
            try:
                return [self.message_item % {
                            "title": item["title"],
                            "link": item["link"],
                        }
                        for item in feed_parse(link)["items"][0:max_item]]

            except urllib_error.URLError:
                return self.message_error

        return self.message_missing
