# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Random
from random import randint

# Waka
from waka.module import WakaModule


class Module(WakaModule):

    def __init__(self, *args, **kargs):
        """ Constructor
        """

        super().__init__(*args, **kargs)

        try:
            path = self.get_internal_file("data.txt")

            with path.open(encoding="utf8") as pipe:
                self.sentences = [text.strip() for text in pipe.readlines()]

        except Exception:
            self.sentences = list()

    def call(self):
        """ Call this method from channels

        Returns
        -------
        str or None
            Response message
        """

        messages = self.response

        # Retrieve a random message from storage
        if len(messages) == 1 or messages[1] == "random":
            index = randint(0, len(self.sentences) - 1)

        # Show how many message has been stored
        elif messages[1] == "number":
            return self.message_number % {
                "number": len(self.sentences)
            }

        # Show a specific message from storage
        else:
            if messages[1] == "show":
                if len(messages) == 2:
                    return self.message_wrong_show_argument

                number = messages[2]

            else:
                number = messages[1]

            try:
                index = int(number) - 1

            except ValueError:
                return self.message_unknown

            if index not in range(0, len(self.sentences)):
                return self.message_wrong_range % {
                    "length": len(self.sentences) + 1,
                }

        return self.sentences[index] % {
            "user": self.current_user,
        }
