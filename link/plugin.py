# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Network
from html.parser import HTMLParser

from urllib.request import Request, urlopen

# Regex
from re import compile as re_compile

# Waka
from waka.module import WakaModule


class TitleParser(HTMLParser):

    def __init__(self):
        """ Constructor
        """

        HTMLParser.__init__(self)

        # Manage title tag detection
        self.__match = False

        # Manage title tag content
        self.title = str()

    def handle_starttag(self, tag, attributes):
        """ Handle the start of a tag

        Parameters
        ----------
        tag : str
            Tag name
        attributes : list
            Attributes list
        """

        # Detect title tag
        self.__match = (tag == "title" and not attributes)

    def handle_data(self, data):
        """ Process arbitrary data

        Parameters
        ----------
        data : str
            Data to process
        """

        if self.__match and not self.title:
            self.title = data

            self.__match = False


class Module(WakaModule):

    def __on_open_link(self, parser, link):
        """ Open a link and retrieve title

        Parameters
        ----------
        parser : TitleParser
            Custom HTMLParser instance
        link : str
            Link to parse

        Returns
        -------
        str or None
            Parsed title
        """

        try:
            request = Request(link, headers={"User-Agent": "URLParser"})

            pipe = urlopen(request, timeout=5).read()

            results = pipe.decode("utf-8", "replace").replace('\n', '')

            if results:
                parser.feed(results)

                if parser.title:
                    return self.message_text % str(parser.title)

            parser.reset()

        except Exception:
            pass

        return None

    def listen(self):
        """ Listen messages from channels

        Returns
        -------
        str
            Response message
        """

        text = list()

        if len(self.response) > 1:

            try:
                # Create HTML parser
                parser = TitleParser()

                # Detect an URL in current message
                url_pattern = re_compile(self.variable_link)

                for link in url_pattern.findall(self.response.lower()):

                    if link:
                        result = self.__on_open_link(parser, link)

                        if result is not None:
                            text.append(result)

                parser.close()

            except Exception:
                pass

        return text
