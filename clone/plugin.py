# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Waka
from waka.module import WakaModule


class Module(WakaModule):

    def call(self):
        """ Call this method from channels

        Returns
        -------
        str or None
            Response message
        """

        if not self.config.has_section("websites"):
            return self.message_empty

        if len(self.response) <= 1:
            return self.message_available % (
                ", ".join(self.config.options("websites")))

        # Retrieve website link
        link = self.config.get("websites", self.response[1].lower())

        if link is None:
            return self.message_missing

        return link
