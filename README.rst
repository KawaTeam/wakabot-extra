WakaBot - Extra modules collection
==================================
All the modules which are too strange to figure on the main repository.

Installation
------------
To install the wakabot, use `pip`::

    pip install .

Dependencies
------------
WakaBot-extra only needs the following dependencies to work:

- Python 3.6
- python3-feedparser (for rss module)

Available modules
-----------------

Caution
~~~~~~~
Some of your users said the same word everytime ? It's annoying, but less than
this module. When the word is said by someone, the module send a message to
inform this person.

**Command**::

    .caution

Clone
~~~~~
You works on an incredible free software project and someone wants to have
a clone link ? Tadaa, a module is available for his needs !

**Command**::

    .clone website

**Preinstall**:
Ensure to add a ``plugin.data`` file in module directory, like this ::

    [websites]
    wakabot = git clone https://framagit.org/KawaTeam/wakabot
    wakabot-extra = git clone https://framagit.org/KawaTeam/wakabot-extra

Link
~~~~
You found an amazing website and want to share it with your buddies ? Go on,
put the URL link on your message and the bot going to retrieve automatically
the website title !

RSS
~~~
You like to procrastinate on the Web but your are too lazy to check your
favorite RSS Feeds Reader ? This command was made for you !

**Command**::

    .rss website [items]

**Preinstall**:
Ensure to add a ``plugin.data`` file in module directory, like this ::

    [websites]
    framablog = https://framablog.org/feed/
    linuxfr = https://linuxfr.org/news.atom

Slap
~~~~
You are angry with someone ? Don't panic and use the slap command ! With this
command, you can enjoy a debate with powerful arguments !

**Command**::

    .slap [username]

Time clock
~~~~~~~~~~
You want to go home but you just arrived at the office ? With this module, you
can know how many time left before you can achieved this dream !

**Command**::

    .date [username] [(+|-)time]

**Preinstall**:
Ensure to add a ``plugin.data`` file in module directory, like this ::

    [username1]
    hour = 12
    minute = 00

    [username2]
    hour = 24
    minute = 00
