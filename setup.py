#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup
from setuptools import find_packages

setup(
    name='Wakabot-extra',
    version='0.1.0',
    author='PacMiam',
    author_email='pacmiam@tuxfamily.org',
    description='All the modules which are too strange to figure on the main '
                'repository',
    keywords='irc bot modules',
    url='https://framagit.org/KawaTeam/wakabot/',
    project_urls={
        'Source': 'https://framagit.org/KawaTeam/wakabot-extra',
        'Tracker': 'https://framagit.org/KawaTeam/wakabot-extra/issues',
    },
    packages=find_packages(exclude=['tools', 'test']),
    include_package_data=True,
    python_requires='~= 3.6',
    install_requires=[
        'feedparser',   # RSS module
    ],
    license='GPLv3',
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.6',
    ],
)
