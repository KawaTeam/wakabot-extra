# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# Date
from datetime import datetime, timedelta

# Locale
from locale import LC_ALL, setlocale
from locale import Error as locale_Error

# Regex
from re import compile as re_compile

# Waka
from waka.module import WakaModule


class Module(WakaModule):

    def __init__(self, *args, **kargs):
        """ Constructor
        """

        super().__init__(*args, **kargs)

        self.regex = re_compile(
            r'(?P<symbol>[\+\-]{1})(?P<hour>\d+)[h\:](?P<minute>\d+)?[m]?')

        try:
            setlocale(LC_ALL, self.variable_locale)

        except locale_Error:
            pass

    def call(self):
        """ Call this method from channels

        Returns
        -------
        str or None
            Response message
        """

        date_now = datetime.now()

        # Send current datetime
        if len(self.response) == 1:
            return self.message_date % (
                date_now.strftime(self.variable_dateformat))

        # This user did not have any timer
        if not self.config.has_section(self.response[1].lower()):
            return self.message_unknown % self.response[1]

        # Retrieve timer for wanted user
        hour = self.config.getint(self.response[1].lower(), "hour")
        minute = self.config.getint(self.response[1].lower(), "minute")

        seconds = ((date_now.hour * 60) + date_now.minute) * 60

        date_now = timedelta(seconds=(seconds + date_now.second))

        # User add a delta to change his time-clock value
        if len(self.response) > 2:
            result = self.regex.match(self.response[2])

            if result is not None:

                if result.group("hour") is not None:

                    if result.group("symbol") == '+':
                        hour += int(result.group("hour"))

                    elif result.group("symbol") == '-':
                        hour -= int(result.group("hour"))

                if result.group("minute") is not None:

                    if result.group("symbol") == '+':
                        minute += int(result.group("minute"))

                    elif result.group("symbol") == '-':
                        minute -= int(result.group("minute"))

        try:
            timer = timedelta(seconds=(((hour * 60) + minute) * 60)) - date_now

            # There still some time in the timer
            if timer.total_seconds() > 0:
                return self.message_wait % str(timer)

            return self.message_done % str(-timer)

        except OverflowError:
            return self.message_overflow
